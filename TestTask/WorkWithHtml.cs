﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace TestTask
{
    public class WorkWithHtml
    {
       public string GetHtml(string uri)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            request.UserAgent = "My applicartion name";
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.Default, true, 8192))
            {
                return reader.ReadToEnd();
            }
        }

        public List<string> GetHref(string html)
        {
            var web = new HtmlAgilityPack.HtmlDocument();

            web.LoadHtml(html);

            var hrefs = web.DocumentNode.SelectNodes("//a[@href]")
                  .Select(p => p.GetAttributeValue("href", "not found"))
                  .ToList();
            return hrefs;
        }

    }
}
