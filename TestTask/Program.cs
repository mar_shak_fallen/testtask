﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace TestTask
{
    class Program
    {
        static void Main(string[] args)
        {
            WorkWithHtml workWithHtml = new WorkWithHtml();
            Program program = new Program();
            Writer writer = new Writer();

            string url = program.ReadUrl();
            bool isUrl = program.IsUrlValid(url);

            if (isUrl == false)
            {
                Console.WriteLine("You entered an incorrect url");
                return;
            }

            string html = workWithHtml.GetHtml(url);
            var hrefs = workWithHtml.GetHref(html);

            writer.WriteInFile(hrefs);
            writer.ConsoleWrite(hrefs);
        }

        private string ReadUrl()
        {
            Console.WriteLine("Please, enter webSite url");
            string url = Console.ReadLine();
            return url;
        }

        private bool IsUrlValid(string url)
        {
            string pattern = @"^(http|https|ftp|)\://|[a-zA-Z0-9\-\.]+\.[a-zA-Z](:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\-\._\?\,\'/\\\+&amp;%\$#\=~])*[^\.\,\)\(\s]$";
            Regex reg = new Regex(pattern, RegexOptions.Compiled | RegexOptions.IgnoreCase);
            return reg.IsMatch(url);
        }
    }
}
