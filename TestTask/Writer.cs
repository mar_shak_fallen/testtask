﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestTask
{
    public class Writer
    {
        public void ConsoleWrite(List<string> hrefs)
        {
            foreach(var href in hrefs.Take(20))
            {
                Console.WriteLine(href);
            }
        }

        public void WriteInFile(List<string> hrefs)
        {
            StreamWriter sw = new StreamWriter("hrefs.txt");

            foreach(var href in hrefs.Take(20))
            {
                sw.WriteLine(href);
            }
            sw.Close();
        }
    }
}
